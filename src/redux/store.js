import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import storage from '@react-native-community/async-storage';
import rootReducer from './RootReducer';
const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: []
};

const pReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(pReducer);

const persistor = persistStore(store);

export {store, persistor};

export default null;
