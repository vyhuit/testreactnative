const ACTION_LOGIN = 'LOGIN_USER';

const loginUser = (
  username,
  password,
  isLogin = true,
) => ({
  type: ACTION_LOGIN,
  username,
  password,
  isLogin,
});

export {loginUser, ACTION_LOGIN};

const initState = {
    username: '',
    password: '',
    flag: 0
};

export default (state = initState, action) => {
  switch (action['type']) {
    case ACTION_LOGIN:
      return {
        ...state,
        username: action['username'],
        password: action['password'],
        flag: state['flag'] + 1,
      };
    default:
      return state;
  }
};
