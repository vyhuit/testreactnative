import {combineReducers} from 'redux';
import loginUser from "./login/loginUser";
const rootReducer = combineReducers({
    login: loginUser
});

export default rootReducer;