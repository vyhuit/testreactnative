import React, {useState, useEffect} from 'react';
import {Button, Text, TextInput, View} from 'react-native';
import {useDispatch, connect} from 'react-redux'
import {loginUser} from '../../redux/login/loginUser'


const LoginScreen = props => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch(null)
  const {user, pass} = props

  useEffect(() => {
    console.log("vo ne")
  }, [props['flag']]);

  const onLogin = () => {
    dispatch(loginUser(username, password))
  };
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 15,
        backgroundColor: '#ddaa2d'
      }}>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        secureTextEntry
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
      />
      <Button style={{marginVertical: 15}} title="Login" onPress={onLogin} />
      <Text>{`${user}-${pass}`}</Text>
    </View>
  );
};

const mapStateToProps = state => ({
  user: state.login.username,
  pass: state.login.password,
  flag: state.login.flag
});

export default connect(mapStateToProps, null)(LoginScreen);
