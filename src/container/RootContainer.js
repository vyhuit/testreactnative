import React from 'react';
import {StatusBar, View} from 'react-native';
import {Styles} from '../assets/styles';
import {RootNavigation} from './navigation/RootNavigation';

class RootContainer extends React.Component {
  state = {
    flagIndicator: false,
    flagMessage: 0,
    flagTextMessage: 0,
    flagWarning: 0,
  };
  UNSAFE_componentWillReceiveProps = nextProp => {
    if (this.props.flagIndicator != nextProp.flagIndicator) {
      this.setState({flagIndicator: nextProp.flagIndicator});
    }
    if (this.props.flagMessage.flag != nextProp.flagMessage.flag) {
      this.setState({flagMessage: nextProp.flagMessage.flag});
    }
    if (this.state.flagTextMessage != nextProp.textMessage.flag) {
      this.setState({flagTextMessage: nextProp.textMessage.flag});
    }
    if (this.state.flagWarning != nextProp.flagWarning.flag) {
      this.setState({flagWarning: nextProp.flagWarning.flag}, () => {
        if (nextProp['flagWarning']['message'])
          this.props.showFlagMessage({
            item: nextProp.flagWarning.message,
          });
      });
    }
  };

  render() {
    console.log(2);
    return (
      <View style={[{backgroundColor: 'transparent'}, Styles.container]}>
        <StatusBar animated={true} barStyle="light-content" backgroundColor ="#ddaa2d"/>
        <RootNavigation />
      </View>
    );
  }
}

export default RootContainer;
