import React from 'react';
import {SafeAreaView} from 'react-native';
import RootContainer from './RootContainer';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from '../redux/store';
class App extends React.Component {
  render() {
    console.log(1);
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <SafeAreaView style={{flex: 1}}>
            <RootContainer />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
