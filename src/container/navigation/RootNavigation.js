import { createNavigationContainerRef, NavigationContainer } from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import Home from '../main/Home';
import LoginScreen from '../user/LoginScreen';

const screens = [
  //   {
  //     name: 'LauncherScreen',
  //     component: LauncherScreen,
  //   },
  {
    name: 'LoginScreen',
    component: LoginScreen,
  },
//   {
//     name: 'Home',
//     component: Home,
//   },
];

const Stack = createNativeStackNavigator();

const navigator = createNavigationContainerRef();

export class RootNavigation extends React.Component {
  UNSAFE_componentWillReceiveProps = nextProps => {
    if (this.props.stack != nextProps.stack) {
      const {screen, data} = nextProps;
      navigator.navigate(screen, data);
    }

    if (this.props.resetStack != nextProps.resetStack) {
      const {screen, data} = nextProps;
      navigator.reset({
        index: 0,
        routes: [{name: screen, data}],
      });
    }

    if (this.props.removeStack != nextProps.removeStack) {
      const {screen, data, index} = nextProps;
      navigator.reset({
        index,
        routes: [{name: screen, data}],
      });
    }
  };

  render() {
    return (
      <NavigationContainer ref={navigator}>
        <Stack.Navigator initialRouteName={'LoginScreen'}>
          {screens.map((e, idx) => {
            return (
              <Stack.Screen
                key={idx}
                name={e.name}
                options={{
                //   transitionSpec,
                  headerShown: false,
                  orientation: 'portrait_up',
                  animation:
                    e.name == 'LoginScreen'
                      ? 'slide_from_left'
                      : 'slide_from_right',
                }}
                component={e.component}
              />
            );
          })}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
